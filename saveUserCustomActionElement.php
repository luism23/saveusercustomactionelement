<?php
/*
Plugin Name: Template Generate Pdf
Plugin URI: https://gitlab.com/luism23/saveusercustomactionelement
Description: Template Generate Pdf
Author: LuisMiguelNarvaez
Version: 1.0.1
Author URI: https://gitlab.com/luism23
License: GPL
 */

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/luism23/saveusercustomactionelement',
	__FILE__,
	'saveUserCustomActionElement'
);
$myUpdateChecker->setAuthentication('glpat-puSVzsHaWSc66qa_rVr1');
$myUpdateChecker->setBranch('master');

define("SUCAE_PATH",plugin_dir_path(__FILE__));
define("SUCAE_URL",plugin_dir_url(__FILE__));

function SUCAE_get_version() {
    $plugin_data = get_plugin_data( __FILE__ );
    $plugin_version = $plugin_data['Version'];
    return $plugin_version;
}

add_action( 'elementor_pro/init', function() {
    require SUCAE_PATH."elementorSave.php";

	// Instantiate the action class
	$sendy_action = new SaveUserFormAction();

	// Register the action with form widget
	\ElementorPro\Plugin::instance()->modules_manager->get_modules( 'forms' )->add_form_action( $sendy_action->get_name(), $sendy_action );
});


