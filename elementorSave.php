<?php

class SaveUserFormAction extends \ElementorPro\Modules\Forms\Classes\Action_Base {
	public function get_name() {
		return 'save_user';
	}

	public function get_label() {
		return __( 'Save User', 'elmformaction' );
	}


	/**
	 * @param \ElementorPro\Modules\Forms\Classes\Form_Record $record
	 * @param \ElementorPro\Modules\Forms\Classes\Ajax_Handler $ajax_handler
	 */
	public function run( $record, $ajax_handler ) {
		$settings = $record->get( 'form_settings' );

		$ajax_handler->add_response_data( true, 'Usuario Registrado'.json_encode($settings));
        $ajax_handler->add_error_message( 'Ocurrio un error' );
	}

	public function on_export($element) {}
}
?>